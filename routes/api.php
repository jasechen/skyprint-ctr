<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// ===
Route::pattern('id', '[0-9]+');

Route::group(['prefix' => 'file'], function () use ($router) {
    $router->post('', 'File@upload');
});


Route::group(['prefix' => 'order'], function () use ($router) {
    $router->post('', 'Order@create');
    $router->get('{id}', 'Order@find');
    $router->get('phone/{phone}', 'Order@findByPhone');
    $router->get('list/{order_way?}/{page?}', 'Order@findAll');
});


Route::group(['prefix' => 'machine'], function () use ($router) {
    $router->post('', 'Machine@create');
    $router->get('{id}', 'Machine@findById');
    $router->get('code/{code}', 'Machine@findByCode');
    $router->delete('{id}', 'Machine@delete');
    $router->put('{id}', 'Machine@update');
    $router->get('list/{order_way?}/{page?}', 'Machine@findAll');
});

Route::group(['prefix' => 'material'], function () use ($router) {
    $router->post('', 'Material@create');
    $router->get('{id}', 'Material@findById');
    $router->get('code/{code}', 'Material@findByCode');
    $router->delete('{id}', 'Material@delete');
    $router->put('{id}', 'Material@update');
    $router->get('list/{order_way?}/{page?}', 'Material@findAll');
});


Route::group(['prefix' => 'sizetype'], function () use ($router) {
    $router->post('', 'SizeType@create');
    $router->get('{id}', 'SizeType@findById');
    $router->get('code/{code}', 'SizeType@findByCode');
    $router->delete('{id}', 'SizeType@delete');
    $router->put('{id}', 'SizeType@update');
    $router->get('list/{order_way?}/{page?}', 'SizeType@findAll');
});


Route::group(['prefix' => 'log'], function () use ($router) {
    $router->get('files/{id}','Log@filesStatusLog');
    $router->get('cronjob', 'Log@cronStatusLog');
});

Route::group(['prefix' => 'cronjob'], function () use ($router) {

    $router->get('orderfile','CronJob@orderFiles');

    $router->get('pushfile','CronJob@pushFiles');
    $router->get('pullfile', 'CronJob@pullFiles');
    $router->get('finishfile', 'CronJob@finishFiles');
    $router->get('deliveryfile', 'CronJob@deliveryFiles');
});


Route::group(['prefix' => 'session'], function () use ($router) {
    $router->post('init',  'Session@init');
    $router->get('{code}', 'Session@findByCode');
});




