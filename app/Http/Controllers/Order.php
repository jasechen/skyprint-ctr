<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\BaseServ;
use App\Services\FileServ;
use App\Services\FilesLogServ;
use App\Services\SessionServ;
use App\Services\OrderServ;
use App\Services\OrderItemServ;
use App\Services\MachineServ;
use App\Services\MaterialServ;
use App\Services\SizeTypeServ;


class Order extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->sessionServ  = app(SessionServ::class);
        $this->orderServ = app(OrderServ::class);
        $this->orderItemServ = app(OrderItemServ::class);
        $this->machineServ = app(MachineServ::class);
        $this->materialServ = app(MaterialServ::class);
        $this->sizeTypeServ = app(SizeTypeServ::class);
        $this->fileServ = app(FileServ::class);
        $this->filesLogServ = app(FilesLogServ::class);
        $this->baseServ = app(BaseServ::class);
    } // END function


    /**
     * create
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function create(Request $request)
    {
        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        //判斷空值 order
        $filedArr = $request->all();
        $tmpArr =['machine_id','status','payment','phone'];
        $this->baseServ->isNoEmpty($filedArr,$tmpArr);

        $machineId = $request->input('machine_id');

        $machineDatum = $this->machineServ->findById($machineId);
        if ($machineDatum->isEmpty()) {
            $code = 409;
            $comment = 'machine_id error';
            Jsonponse::fail($comment, $code);
        } // END if


        //end order
        //判斷空值 order_datas ＋ get orderItem 值
        $orders = $request->input('orders');
        $tmpArr =['materials_id','size_types_id','file_id','num','price'];
        foreach ($orders as $key => $value){
            $this->baseServ->isNoEmpty($value,$tmpArr);
            $materialsId = $value['materials_id'];
            $sizeTypeId = $value['size_types_id'];
            $fileId = $value['file_id'];

            $materialDatum = $this->materialServ->findById($materialsId);

            if ($materialDatum->isEmpty()) {
                $code = 409;
                $comment = 'materials_id error';
                Jsonponse::fail($comment, $code);
            } // END if
            $orders[$key]['price'] = $materialDatum->first()->price;


            $sizeTypeServDatum = $this->sizeTypeServ->findById($sizeTypeId);
            if ($sizeTypeServDatum->isEmpty()) {
                $code = 409;
                $comment = 'size_types_id error';
                Jsonponse::fail($comment, $code);
            } // END if
            if($sizeTypeId != '99'){
                $tmpArr =['width','height'];
                $this->baseServ->isNoEmpty($value,$tmpArr);
                $orders[$key]['width'] =  $sizeTypeServDatum->first()->width;
                $orders[$key]['height'] = $sizeTypeServDatum->first()->height;
            }

            $fileDatum = $this->fileServ->findById($fileId);
            if ($fileDatum->isEmpty()) {
                $code = 409;
                $comment = 'file_id error';
                Jsonponse::fail($comment, $code);
            } // END if
        }
        //end order_datas

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';
            Jsonponse::fail($comment, $code);
        } // END if

        //取order值
        $status = $request->input('status');
        $payment = $request->input('payment');
        $machineId = $request->input('machine_id');
        $note = $request->input('note');
        $phone = $request->input('phone');
        $isPay = $request->input('is_pay','N');
        //end 取order值

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_orders.ORDERS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';
            Jsonponse::fail($comment, $code);
        } // END if

        $paymentValidator = Validator::make(['payment' => $payment],
            ['payment' => ['in:' . implode(',', config('tbl_orders.ORDERS_PAYMENT'))]]
        );

        if ($paymentValidator->fails()) {
            $code = 422;
            $comment = 'payment error';
            Jsonponse::fail($comment, $code);
        } // END if

        $isPayValidator = Validator::make(['is_pay' => $isPay],
            ['is_pay' => ['in:Y,N']]
        );

        if ($isPayValidator->fails()) {
            $code = 422;
            $comment = 'is_pay error';
            Jsonponse::fail($comment, $code);
        } // END if

        if(!is_numeric($phone)){
            $code = 422;
            $comment = 'phone error';
            Jsonponse::fail($comment, $code);
        }

        //建訂單
        $OrderDatum = $this->orderServ->create($machineId,$status,$payment,$note,$phone,$isPay,$sessionCode);
        //訂單id

        $totalPrice = 0;
        //建詳細訂單
        foreach ($orders as $value){
            $materialsId = $value['materials_id'];
            $sizeTypesId = $value['size_types_id'];
            $fileId = $value['file_id'];
            $machineId = $request->input('machine_id');


            $num = $value['num'];
            if(!is_numeric($num) or $num =='0'){
                $code = 422;
                $comment = 'num error';
                Jsonponse::fail($comment, $code);
            }
            $price = $value['price'];
            if(!is_numeric($price) or $price =='0'){
                $code = 422;
                $comment = 'price error';
                Jsonponse::fail($comment, $code);
            }

            $width =  $value['width'];
            $height = $value['height'];

            //算才數
            //換算為公分
            $tmpWidth =null;
            $tmpHeight =null;
            if($width < 600 and $height <600  ){
                if($width < $height){
                    $tmpHeight =60;
                    $tmpWidth = ceil($width/10);
                }else{
                    $tmpHeight = ceil($height/10);
                    $tmpWidth = 60;
                }
            }
            if($width >= 900 or $height >= 900){
                if($width <900){
                    $tmpHeight = ceil($height/10);
                    $tmpWidth =60;
                }
                if($height <900){
                    $tmpWidth = ceil($width/10);
                    $tmpHeight =60;
                }
            }
            if($width >= 900 and $height >= 900){
                if($width <= 1200 and $height <= 1200){
                    $tmpWidth =120;
                    $tmpHeight =120;
                }
            }

            if($materialsId == '1'){
                $tmpWidth = ceil(900/10);
                $tmpHeight = ceil($height/10);
            }

            if($tmpWidth == null or $tmpHeight ==null){
                $tmpWidth = ceil($width/10);
                $tmpHeight = ceil($height/10);
            }

            $tmpSize = $tmpWidth * $tmpHeight;
            $sNum = ceil($tmpSize / 900);
            //end 算才數

            $note = $value['note'];

            $filePriceTotal = $this->orderItemServ->getPrice($num,$price,$sNum);
            $totalPrice += $filePriceTotal;

            //move rename file
            $materialDatum = $this->materialServ->findById($materialsId);
            $materialName = $materialDatum->first()->short_name;

            $sizeTypeServDatum = $this->sizeTypeServ->findById($sizeTypeId);
            $sizeTypeName = $sizeTypeServDatum->first()->short_name;

            $machineDatum = $this->machineServ->findById($machineId);
            $machineName = $machineDatum->first()->short_name;

            $fileDatum = $this->fileServ->findById($fileId);
            $fileExtension = $fileDatum->first()->extension;
            $filename = $fileDatum->first()->filename;

            $file = $filename.'.'.$fileExtension;
            $originalFileName = $fileDatum->first()->original_file_name;
            $tmpFile = explode(".",$originalFileName);
            $originalFileName = $tmpFile[0];

            $newFileName = $filename.'_'.$materialName.'_'.$phone.'_'.$tmpHeight.'X'.$tmpWidth.'_'.$price.'元_'.$machineName.'__'.$originalFileName;


            $materialDatum = $this->materialServ->findById($materialsId);
            $sizeTypeServDatum = $this->sizeTypeServ->findById($sizeTypeId);

            if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_ORDER'))) {
                    $tFolder ='';
                    $exists = Storage::disk('s3-files-order')->exists($tFolder . $file);
                    if(empty($exists)){
                        $Uploadexists = Storage::disk('s3-files-upload')->exists($tFolder . $file);
                        if($Uploadexists ==1){
                            $changeDone = Storage::disk('s3-files-upload')->move($file, $newFileName.'.'.$fileExtension);
                            if($changeDone ==1){
                                $getFile = Storage::disk('s3-files-upload')->get($tFolder . '/' . $newFileName.'.'.$fileExtension);
                                Storage::disk('s3-files-order')->put($newFileName.'.'.$fileExtension,$getFile);
                            }
                        }
                    }

            } // END if

            //end move rename file
            $OrderItemDatum = $this->orderItemServ->create($OrderDatum->first()->id,$materialsId,$sizeTypesId,$fileId,$num,$price,$width,$height,$note,$filePriceTotal);

            $updatedata = ['filename' => $newFileName ,'status' =>'order',
            'order_id' =>$OrderDatum->first()->id,
            'order_items_id' =>$OrderItemDatum->first()->id
            ];
            $updateWhere = ['id' => $fileDatum->first()->id,'status' =>'upload'];
            $tmpFileDatum = $this->fileServ->update($updatedata,$updateWhere);


            $getFilesWhere = ['id' => $fileDatum->first()->id,'status' =>'order'];
            $tmpFileDatum = $this->fileServ->fetchDatum($getFilesWhere);

            //寫log
            if($tmpFileDatum->isNotEmpty()){
                $status = 'order';
                $data =array();
                $data['filename'] = $filename;
                $data['file_id'] = $fileDatum->first()->id;
                $data['status'] = $status;
                $filesLogDatum = $this->filesLogServ->create($data);

                if ($filesLogDatum->isEmpty()) {
                    $code = 500;
                    $comment = 'create error';

                    Jsonponse::fail($comment, $code);
                } // END if
            }
            //end 寫log
        }


        //update 總價
        $updatedata = ['order_price_total' => $totalPrice];
        $updateWhere = ['id' => $OrderDatum->first()->id];
        $OrderDatum = $this->orderServ->update($updatedata,$updateWhere);

        $resultData = ['session' => $sessionCode, 'order' => $OrderDatum->first()];

        Jsonponse::success('create success', $resultData);
    } // END function


    /**
     * update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderDatum = $this->orderServ->findById($id);

        if ($orderDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $name   = $request->input('name');
        $cover   = $request->input('cover');
        $status   = $request->input('status');

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');

        $updateData = $updateSeoData = [];

        if (!empty($name) AND $name != $orderDatum->first()->name) {
            $updateData['name'] = $name;
        } // END if

        if (!empty($cover) AND $cover != $orderDatum->first()->cover) {
            $fileDatum = $this->fileServ->findByFilename($cover);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'cover error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['cover'] = $cover;
        } // END if

        if (!empty($status) AND $status != $orderDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_categories.CATEGORIES_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if

       if (!empty($slug) AND $slug != $orderDatum->first()->slug) {
            $categorySeoDatum = $this->categorySeoServ->findBySlug($slug);

            if ($categorySeoDatum->isNotEmpty() AND $categorySeoDatum->first()->category_id != $id) {
                $code = 409;
                $comment = 'slug error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateSeoData['slug'] = $slug;
        } // END if

        if (!empty($excerpt) AND $excerpt != $orderDatum->first()->excerpt) {
            $updateSeoData['excerpt'] = $excerpt;
        } // END if

        if (!empty($ogTitle) AND $ogTitle != $orderDatum->first()->og_title) {
            $updateSeoData['og_title'] = $ogTitle;
        } // END if

        if (!empty($ogDescription) AND $ogDescription != $orderDatum->first()->og_description) {
            $updateSeoData['og_description'] = $ogDescription;
        } // END if

        if (!empty($metaTitle) AND $metaTitle != $orderDatum->first()->meta_title) {
            $updateSeoData['meta_title'] = $metaTitle;
        } // END if

        if (!empty($metaDescription) AND $metaDescription != $orderDatum->first()->meta_description) {
            $updateSeoData['meta_description'] = $metaDescription;
        } // END if

        if (!empty($coverTitle) AND $coverTitle != $orderDatum->first()->cover_title) {
            $updateSeoData['cover_title'] = $coverTitle;
        } // END if

        if (!empty($coverAlt) AND $coverAlt != $orderDatum->first()->cover_alt) {
            $updateSeoData['cover_alt'] = $coverAlt;
        } // END if


        if (empty($updateData) AND empty($updateSeoData)) {
            $code = 400;
            $comment = 'updateData / updateSeoData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateFail = $updateSeoFail = false;

        if (!empty($updateData)) {
            $orderDatum = $this->orderServ->update($updateData, ['id' => $id]);

            if (empty($orderDatum) OR $orderDatum->isEmpty()) {
                $updateFail = true;
            } else {
                if (!empty($updateData['cover'])) {
                    $updatedata = ['parent_id' => $orderDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
                    $updateWhere = ['id' => $fileDatum->first()->id];

                    $this->fileServ->update($updatedata, $updateWhere);

                    $this->fileServ->moveFileTo($cover, 'category/' . $orderDatum->first()->id, 'category');

                    if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_GALLERY'))) {
                        $filePaths = $this->fileServ->findLinks($cover);

                        foreach ($filePaths as $filePath) {
                            $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                            $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                            $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                            if (empty($exists)) {
                                $sFilename = public_path($filePath);
                                $sFilename = new File($sFilename);
                                Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                            } // END if
                        } // END foreach
                    } // END if

                } // END if
            } // END if
        } // END if

        if (!empty($updateSeoData)) {
            $categorySeoDatum = $this->categorySeoServ->findByCategoryId($id);

            if ($categorySeoDatum->isNotEmpty()) {
                $categorySeoDatum = $this->categorySeoServ->update($updateSeoData, ['id' => $categorySeoDatum->first()->id]);

                if (empty($categorySeoDatum) OR $categorySeoDatum->isEmpty()) {
                    $updateSeoFail = true;
                } // END if
            } // END if
        } // END if

        if (!empty($updateFail) OR !empty($updateSeoFail)) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderDatum = $this->orderServ->findById($id);

        if ($orderDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateData = ['status' => config('tbl_categories.CATEGORIES_STATUS_DELETE')];
        $orderDatum = $this->orderServ->update($updateData, ['id' => $id]);

        if ($orderDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderDatum = $this->orderServ->findById($id);

        if ($orderDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        // $orderDatum->first()->fileslink = '';
        // if (!empty($orderDatum->first()->file_id)) {
        //     $fileslink = $this->fileServ->findLinks($orderDatum->first()->file_id);
        //     $orderDatum->first()->fileslink = $fileslink;
        // } // END if else

        //  $orderDatum->first()->s3_url = env('AWS_BUCKET_URL');
        //  $orderDatum->first()->url_path = empty($orderDatum->first()->slug) ? '' : '/' . $orderDatum->first()->slug . '-c' . $orderDatum->first()->id;


        // $resultData = ['session' => $sessionCode, 'category' => $orderDatum->first()];
        $resultData = ['order' => $orderDatum->first()];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByPhone
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByPhone(Request $request, $phone)
    {
        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($phone)) {
            $code = 400;
            $comment = 'phone empty';
            Jsonponse::fail($comment, $code);
        } // END if

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderData = $this->orderServ->findByPhone($phone);
        $resultData = ['order' => $orderData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     *
     * @throws
     * @return
     */
    public function findAll(Request $request, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );


        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['o.id' => $orderWay];
        $status = config('tbl_orders.DEFAULT_ORDERS_STATUS');

        $orderData = $this->orderServ->findAll($status, $orderby, $page);

        if ($orderData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        $orderData = $orderData->each(function ($item, $key) {
            $orderItemsDatum = $this->orderServ->findByIdToAll($item->id);
            $item->orders =  $orderItemsDatum;
        });
        //  print_r($orderData);
        //  exit;
        // //$resultData = ['orders' => $orderData];

        Jsonponse::success('find success', $orderData->toArray());
    } // END function


} // END class
