<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\BaseServ;
use App\Services\SessionServ;
use App\Services\CronJobServ;
use App\Services\FileServ;
use App\Services\FilesLogServ;
use App\Services\CornJobLogServ;
use App\Services\OrderServ;
use App\Services\OrderItemServ;
use App\Services\MachineServ;


class CronJob extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->sessionServ  = app(SessionServ::class);
        $this->cronJobServ = app(CronJobServ::class);
        $this->baseServ = app(BaseServ::class);
        $this->fileServ = app(FileServ::class);
        $this->filesLogServ = app(FilesLogServ::class);
        $this->cornJobLogServ = app(CornJobLogServ::class);
        $this->orderServ = app(OrderServ::class);
        $this->orderItemServ = app(OrderItemServ::class);
        $this->machineServ = app(MachineServ::class);
    } // END function

    /**
     * pullFiles
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function pullFiles(Request $request)
    {
        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_CONVERT'))) {
            $s3Files = Storage::disk('s3-files-convert')->files('/');
            $ftpFiles = Storage::disk('ftp-convert-done')->files('/');

            $ftpServer = Storage::disk('ftp-convert-done');
            $s3Server = Storage::disk('s3-files-convert');

            foreach ($ftpFiles as $fileName) {

                $tmpfileArray = explode('.',$fileName);
                $filefirstName = $tmpfileArray[0];
                $fileDatum = $this->fileServ->findByFilename($filefirstName);

                if($fileDatum->isNotEmpty()){
                    //file move
                    $getFile = $ftpServer->get($fileName);
                    $s3Server->put($fileName,$getFile);
                    //end file move

                    $updatedata = ['status' =>'finish'];
                    $updateWhere = ['id' => $fileDatum->first()->id,'status' =>'pull'];
                    $tmpFileDatum = $this->fileServ->update($updatedata,$updateWhere);
    
                    $getFilesWhere = ['id' => $fileDatum->first()->id,'status' =>'finish'];
                    $tmpFileDatum = $this->fileServ->fetchDatum($getFilesWhere);

                }

                //寫log
                if($tmpFileDatum->isNotEmpty()){
                    $status = 'finish';
                    $data =array();
                    $data['filename'] = $filefirstName;
                    $data['file_id'] = $fileDatum->first()->id;
                    $data['status'] = $status;
                    $filesLogDatum = $this->filesLogServ->create($data);

                    if ($filesLogDatum->isEmpty()) {
                        $code = 500;
                        $comment = 'create error';
                        Jsonponse::fail($comment, $code);
                    } // END if
                }
            //end 寫log
            } // END foreach

            $status = 'success';
            $jobStatus = 'pull';
            $data =array();

            $data['status'] = $status;
            $data['job_status'] = $jobStatus;
            $cornJobLogDatum = $this->cornJobLogServ->create($data);

            if ($cornJobLogDatum->isEmpty()) {
                $code = 500;
                $comment = 'create error';

                Jsonponse::fail($comment, $code);
            } // END if

        } // END if

        $resultData = ['CronJob' => 'PullFilesDone'];
        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * pushFiles
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $machine_id
     *
     * @throws
     * @return
     */
    public function pushFiles(Request $request)
    {
        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_UPLOAD'))) {
            $s3Files = Storage::disk('s3-files-upload')->files('/');
            $ftpServer = Storage::disk('ftp-convert');
            $s3Server = Storage::disk('s3-files-upload');
            foreach ($s3Files as $fileName) {

                $tmpfileArray = explode('.',$fileName);
                $filefirstName = $tmpfileArray[0];
                $fileDatum = $this->fileServ->findByFilename($filefirstName);

                if($fileDatum->isNotEmpty()){
                    //file move s3Server
                    $getFile = $s3Server->get($fileName);
                    $ftpServer->put($fileName,$getFile);
                    //end file move
                    $updatedata = ['status' =>'pull'];
                    $updateWhere = ['id' => $fileDatum->first()->id,'status' =>'push'];
                    $tmpFileDatum = $this->fileServ->update($updatedata,$updateWhere);

                    $getFilesWhere = ['id' => $fileDatum->first()->id,'status' =>'pull'];
                    $tmpFileDatum = $this->fileServ->fetchDatum($getFilesWhere);

                }

                //寫log
                if($tmpFileDatum->isNotEmpty()){
                    $status = 'pull';
                    $data =array();
                    $data['filename'] = $filefirstName;
                    $data['file_id'] = $fileDatum->first()->id;
                    $data['status'] = $status;
                    $filesLogDatum = $this->filesLogServ->create($data);

                    if ($filesLogDatum->isEmpty()) {
                        $code = 500;
                        $comment = 'create error';
                        Jsonponse::fail($comment, $code);
                    } // END if
                }
            //end 寫log
            } // END foreach

            $status = 'success';
            $jobStatus = 'push';
            $data =array();

            $data['status'] = $status;
            $data['job_status'] = $jobStatus;
            $cornJobLogDatum = $this->cornJobLogServ->create($data);

            if ($cornJobLogDatum->isEmpty()) {
                $code = 500;
                $comment = 'create error';

                Jsonponse::fail($comment, $code);
            } // END if

        } // END if

        $resultData = ['CronJob' => 'PushFilesDone'];
        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * orderFiles
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $machine_id
     *
     * @throws
     * @return
     */
    public function orderFiles(Request $request)
    {
        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_UPLOAD'))) {
            $s3Files = Storage::disk('s3-files-upload')->files('/');
            $convertForder = config('tbl_cronjob.CRON_JOB_FTP_CONVERT_PATH');
            foreach ($s3Files as $fileName) {
                $tmpfileArray = explode('.',$fileName);
                $filefirstName = $tmpfileArray[0];
                $fileDatum = $this->fileServ->findByFilename($filefirstName);

                if($fileDatum->isNotEmpty()){
                    $updatedata = ['status' =>'push'];
                    $updateWhere = ['id' => $fileDatum->first()->id,'status' =>'order'];
                    $tmpFileDatum = $this->fileServ->update($updatedata,$updateWhere);

                    $getFilesWhere = ['id' => $fileDatum->first()->id,'status' =>'push'];
                    $tmpFileDatum = $this->fileServ->fetchDatum($getFilesWhere);
                }

                //寫log
                if($tmpFileDatum->isNotEmpty()){
                    $status = 'push';
                    $data =array();
                    $data['filename'] = $filefirstName;
                    $data['file_id'] = $fileDatum->first()->id;
                    $data['status'] = $status;
                    $filesLogDatum = $this->filesLogServ->create($data);

                    if ($filesLogDatum->isEmpty()) {
                        $code = 500;
                        $comment = 'create error';
                        Jsonponse::fail($comment, $code);
                    } // END if
                }
            //end 寫log
            } // END foreach

            $status = 'success';
            $jobStatus = 'order';
            $data =array();

            $data['status'] = $status;
            $data['job_status'] = $jobStatus;
            $cornJobLogDatum = $this->cornJobLogServ->create($data);

            if ($cornJobLogDatum->isEmpty()) {
                $code = 500;
                $comment = 'create error';
                Jsonponse::fail($comment, $code);
            } // END if

        } // END if

        $resultData = ['CronJob' => 'OrderFilesDone'];
        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * finishFiles
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function finishFiles(Request $request)
    {
        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_CONVERT'))) {
            $s3Files = Storage::disk('s3-files-convert')->files('/');
            $s3Server = Storage::disk('s3-files-convert');
            $s3DeliveryServer = Storage::disk('s3-files-delivery');

            foreach ($s3Files as $fileName) {

                $tmpfileArray = explode('.',$fileName);
                $filefirstName = $tmpfileArray[0];
                $fileDatum = $this->fileServ->findByFilename($filefirstName);

                if($fileDatum->isNotEmpty()){
                    // $this->orderServ;
                    // $this->orderItemServ;
                    $orderId = $fileDatum->first()->order_id;
                    $orderDatum = $this->orderServ->findById($orderId);
                    if($orderDatum->isNotEmpty()){
                        $machineDatum = $this->machineServ->findById($orderDatum->first()->machine_id);
                    }
                    if($machineDatum->isNotEmpty()){
                        $forderName =  $machineDatum->first()->forder_name;
                    }
                    //抓取這個檔案的 order_id order_item_id
                    //file move
                    $getFile = $s3Server->get($fileName);
                    $s3DeliveryServer->put($forderName.'/'.$fileName,$getFile);
                    //end file move

                    $updatedata = ['status' =>'print'];
                    $updateWhere = ['id' => $fileDatum->first()->id,'status' =>'finish'];
                    $tmpFileDatum = $this->fileServ->update($updatedata,$updateWhere);

                    $getFilesWhere = ['id' => $fileDatum->first()->id,'status' =>'print'];
                    $tmpFileDatum = $this->fileServ->fetchDatum($getFilesWhere);
                }

                //寫log
                if($tmpFileDatum->isNotEmpty()){
                    $status = 'print';
                    $data =array();
                    $data['filename'] = $filefirstName;
                    $data['file_id'] = $fileDatum->first()->id;
                    $data['status'] = $status;
                    $filesLogDatum = $this->filesLogServ->create($data);

                    if ($filesLogDatum->isEmpty()) {
                        $code = 500;
                        $comment = 'create error';
                        Jsonponse::fail($comment, $code);
                    } // END if
                }
            //end 寫log
            } // END foreach

            $status = 'success';
            $jobStatus = 'finish';
            $data =array();

            $data['status'] = $status;
            $data['job_status'] = $jobStatus;
            $cornJobLogDatum = $this->cornJobLogServ->create($data);

            if ($cornJobLogDatum->isEmpty()) {
                $code = 500;
                $comment = 'create error';

                Jsonponse::fail($comment, $code);
            } // END if

        } // END if


        $resultData = ['CronJob' => 'finishFilesDone'];
        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * finishFiles
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function deliveryFiles(Request $request)
    {
        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_DELIVERY'))) {

            $machineData = $this->machineServ->findAll();

            foreach($machineData as $machine){
                config(['filesystems.disks.ftp-sky.host' => $machine->ftp_host]);
                config(['filesystems.disks.ftp-sky.username' => $machine->ftp_username]);
                config(['filesystems.disks.ftp-sky.password' => $machine->ftp_password]);
                //假定ftp 資料夾
                config(['filesystems.disks.ftp-sky.root' => $machine->forder_name]);

                $s3Files = Storage::disk('s3-files-delivery')->files($machine->forder_name);
                $status = null;
                $fileData = null;
                if(count($s3Files) !=0){
                    $status ='print';
                    $fileData = $this->fileServ->findByMachineIdAndStatus($status,$machine->id);
                }
                $ftpServer = Storage::disk('ftp-sky');
                $s3Server = Storage::disk('s3-files-delivery');

                if(!empty($fileData))
                foreach ($fileData as $fileValue) {
                    $fileName = $fileValue->filename.'.'.$fileValue->extension;

                    $pathFileName = $machine->forder_name.'/'.$fileName;

                    $getFile = $s3Server->get($pathFileName);

                    //file move
                    $ftpServer->put($fileName,$getFile);
                    //end file move

                    $updatedata = ['status' =>'done'];
                    $updateWhere = ['id' => $fileValue->id,'status' =>'print'];
                    $tmpFileDatum = $this->fileServ->update($updatedata,$updateWhere);

                    $getFilesWhere = ['id' => $fileValue->id,'status' =>'done'];
                    $tmpFileDatum = $this->fileServ->fetchDatum($getFilesWhere);

                    //寫log
                    if($tmpFileDatum->isNotEmpty()){
                        $status = 'done';
                        $data =array();
                        $data['filename'] = $fileValue->filename;
                        $data['file_id'] = $fileValue->id;
                        $data['status'] = $status;
                        $filesLogDatum = $this->filesLogServ->create($data);

                        if ($filesLogDatum->isEmpty()) {
                            $code = 500;
                            $comment = 'create error';
                            Jsonponse::fail($comment, $code);
                        } // END if
                    }
                    //end 寫log
                } // END foreach
        }// END machine foreach

            $status = 'success';
            $jobStatus = 'print';
            $data =array();

            $data['status'] = $status;
            $data['job_status'] = $jobStatus;
            $cornJobLogDatum = $this->cornJobLogServ->create($data);

            if ($cornJobLogDatum->isEmpty()) {
                $code = 500;
                $comment = 'create error';
                Jsonponse::fail($comment, $code);
            } // END if

        } // END if


        $resultData = ['CronJob' => 'deliveryFilesDone (printFilesDone)'];
        Jsonponse::success('find success', $resultData);
    } // END function

} // END class
