<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\BaseServ;
use App\Services\SessionServ;
use App\Services\SizeTypeServ;



class SizeType extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->sessionServ  = app(SessionServ::class);
        $this->sizeTypeServ = app(SizeTypeServ::class);
        $this->baseServ = app(BaseServ::class);
    } // END function


    /**
     * create
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $filedArr = $request->all();
        $tmpArr =['name','short_name','zone','county','location_name','status','forder_name','code'];
        $this->baseServ->isNoEmpty($filedArr,$tmpArr);
        $code = $request->input('code');
        $code = $request->input('code');
        $forderName = $request->input('forder_name');
        $locationName = $request->input('location_name');

        $sizeTypeDatum = $this->sizeTypeServ->findByCode($code);
        if ($sizeTypeDatum->isNotEmpty()) {
            $code = 404;
            $comment = 'code repart';
            Jsonponse::fail($comment, $code);
        } // END if
        $sizeTypeDatum = $this->sizeTypeServ->findByForderName($forderName);
        if ($sizeTypeDatum->isNotEmpty()) {
            $code = 404;
            $comment = 'forder_name repart';
            Jsonponse::fail($comment, $code);
        } // END if
        $sizeTypeDatum = $this->sizeTypeServ->findByLocationName($locationName);
        if ($sizeTypeDatum->isNotEmpty()) {
            $code = 404;
            $comment = 'location_name repart';
            Jsonponse::fail($comment, $code);
        } // END if

        $createData = $request->input();
        $createDatum = $this->sizeTypeServ->create($createData);

        if ($createDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';
            Jsonponse::fail($comment, $code);
        } // END if

        $resultData = ['session' => $sessionCode, 'material_id' => $createDatum->first()->id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $filedArr = $request->all();
        $tmpArr =['name','short_name','zone','county','location_name','status','forder_name','code'];
        $this->baseServ->isNoEmpty($filedArr,$tmpArr);
        $code = $request->input('code');
        $code = $request->input('code');
        $forderName = $request->input('forder_name');
        $locationName = $request->input('location_name');

        $sizeTypeDatum = $this->sizeTypeServ->findByCode($code);
        if ($sizeTypeDatum->isNotEmpty()) {
            $code = 404;
            $comment = 'code repart';
            Jsonponse::fail($comment, $code);
        } // END if
        $sizeTypeDatum = $this->sizeTypeServ->findByForderName($forderName);
        if ($sizeTypeDatum->isNotEmpty()) {
            $code = 404;
            $comment = 'forder_name repart';
            Jsonponse::fail($comment, $code);
        } // END if
        $sizeTypeDatum = $this->sizeTypeServ->findByLocationName($locationName);
        if ($sizeTypeDatum->isNotEmpty()) {
            $code = 404;
            $comment = 'location_name repart';
            Jsonponse::fail($comment, $code);
        } // END if
        $updateData = $request->input();
        $orderDatum = $this->sizeTypeServ->update($updateData, ['id' => $id]);

        $resultData = ['session' => $sessionCode, 'material_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {
        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $sizeTypeDatum = $this->sizeTypeServ->findById($id);

        if ($sizeTypeDatum->isEmpty()) {
            $code = 404;
            $comment = 'material error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateData = ['status' => config('tbl_material.MATERIAL_STATUS_DELETE')];
        $orderDatum = $this->sizeTypeServ->update($updateData, ['id' => $id]);

        if ($orderDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if

        $resultData = ['session' => $sessionCode, 'material_id' => $id];
        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findById(Request $request, $id)
    {
        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $sizeTypeDatum = $this->sizeTypeServ->findById($id);

        if ($sizeTypeDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        $resultData = ['material' => $sizeTypeDatum->first()];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByCode
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByCode(Request $request, $code)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $sizeTypeDatum = $this->sizeTypeServ->findByCode($code);

        if ($sizeTypeDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        $resultData = ['material' => $sizeTypeDatum->first()];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     *
     * @throws
     * @return
     */
    public function findAll(Request $request, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if
        $orderby = ['id' => $order_way];
        $sizeTypeData = $this->sizeTypeServ->findAll($orderby,$page);

        if ($sizeTypeData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'sizetypes' => $sizeTypeData->all()];

        Jsonponse::success('find success', $resultData);

    } // END function


} // END class
