<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\File as FileSys;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;
use Illuminate\Support\Facades\Storage;

use App\Services\SessionServ;
use App\Services\FileServ;


class File extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ   = app(SessionServ::class);
        $this->fileServ = app(FileServ::class);
    } // END function

    /**
     * Upload the file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function upload(Request $request)
    {

        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if
        $status = $request->input('status', 'upload');
        $isAlive = $this->sessionServ->isAlive($sessionCode);
        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';
            Jsonponse::fail($comment, $code);
        } // END if

        $sessionDatum = $this->sessionServ->findByCode($sessionCode);
        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if



        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';
        //存檔
        $filename = Carbon::now()->timestamp . mt_rand(100000, 999999);
        // Storage::disk('files')->putFileAs('upload', $fileUpload, $filename.'.'.$fileExtension);

        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_UPLOAD'))) {
            Storage::disk('s3-files-upload')->putFileAs('', $fileUpload, $filename.'.'.$fileExtension, 'public');
        } // END if

        //存檔
        $fileDatum = $this->fileServ->create($sessionDatum->first()->id, $filename,$fileOriginalName, $fileExtension, $fileMimeType, $fileSize, $fileType, $status);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $fileLinks = $this->fileServ->findLinks($filename);


        $resultData = ['session'  => $sessionCode, 'file_id'  => $fileDatum->first()->id, 'filename' => $fileDatum->first()->filename, 'file_links' => $fileLinks];

        Jsonponse::success('upload success', $resultData, 201);
    } // END function


} // END class
