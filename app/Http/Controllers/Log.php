<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\BaseServ;

use App\Services\FilesLogServ;
use App\Services\CornJobLogServ;
use App\Services\SessionServ;
use App\Services\FileServ;

class Log extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->baseServ   = app(BaseServ::class);
        $this->filesLogServ   = app(FilesLogServ::class);
        $this->fileServ   = app(FileServ::class);
        $this->cornJobLogServ   = app(CornJobLogServ::class);
        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * filesStatusLog
     *
     * @method Get
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function filesStatusLog(Request $request,$id)
    {
        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        //確認檔案有沒有存在
        /**
         * 確認檔案有沒有存在
         * 傳入狀態 新增一筆
         */
        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findById($id);


        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'files id error';

            Jsonponse::fail($comment, $code);
        } // END if
        $filename = $fileDatum->first()->filename;
        $file_id = $id;
        $status = 'order';
        $data =array();
        $data['filename'] = $filename;
        $data['file_id'] = $file_id;
        $data['status'] = $status;
        $filesLogDatum = $this->filesLogServ->create($data);

        if ($filesLogDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $resultData = ['session' => $sessionCode, 'files_log' => $filesLogDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * CronStatusLog
     *
     * @method Get
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function cronStatusLog(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $status = 'success';
        $jobStatus = 'push';
        $data =array();

        $data['status'] = $status;
        $data['job_status'] = $jobStatus;
        $cornJobLogDatum = $this->cornJobLogServ->create($data);

        if ($cornJobLogDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $resultData = ['session' => $sessionCode, 'cron_job_log' => $cornJobLogDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function

} // END class
