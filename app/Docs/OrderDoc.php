<?php

namespace App\Docs;

    /**
     * 新增訂單
     *
     * @api {POST} /order    01. 新增訂單
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增訂單
     * @apiName PostOrder
     * @apiGroup Orders
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {int}                               [machine_id]                       設備名稱
     * @apiParam {string=enable,disable,delete}                            [status="enable"]                        狀態
     * @apiParam {string=cash,credit}                           [payment="cash"]                        付款方式
     * @apiParam {int}                             phone                        電話
     * @apiParam {string=Y,N}                            [is_pay="N"]                        是否付款
     * @apiParam {array}                            orders                         陣列（詳細訂單）
     * @apiParam {string=enable,disable,delete}           [status="enable"]         狀態
     *
     * @apiParamExample {json} Request
    {
        "machine_id" : "{{machine_id}}",
        "note" : "{{note}}",
        "status" : "{{status}}",
        "payment" : "{{payment}}",
        "phone" : "{{phone}}",
        "is_pay" : "{{is_pay}}",
            "orders": [
                {
                    "materials_id": "{{materials_id}}",
                    "size_types_id": "{{size_types_id}}",
                    "file_id": "{{file_id}}",
                    "width": "{{width}}",
                    "height": "{{height}}",
                    "note": "{{meta_description}}",
                    "num": "{{num}}",
                    "price": "{{price}}",
                    "file_price_total": ""
                },
                {
                    "materials_id": "2",
                    "size_types_id": "{{size_types_id}}",
                    "file_id": "{{file_id}}",
                    "width": "{{width}}",
                    "height": "{{height}}",
                    "note": "{{meta_description}}",
                    "num": "{{num}}",
                    "price": "{{price}}",
                    "file_price_total": ""
                }
            ]
    }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.order          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "create success",
            "data": {
                "session": "1a59f702daee131f876e03baa66b2b84",
                "order": {
                    "id": "52100955696664576",
                    "machine_id": "1",
                    "note": "{{note}}",
                    "status": "enable",
                    "payment": "cash",
                    "is_pay": "N",
                    "phone": "0919900398",
                    "order_price_total": "350",
                    "updated_at": "2018-03-05 18:30:35",
                    "created_at": "2018-03-05 18:30:35"
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}

     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */


    /**
     * 查訂單ById
     *
     * @api {GET} /order/{id}    02.查訂單ById
     * @apiVersion 0.1.0
     * @apiDescription ・ 查訂單ById
     * @apiName GetOrderById
     * @apiGroup Orders
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               [id]                       Id
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.order          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "order": {
                    "id": "51303087310245888",
                    "machine_id": "1",
                    "note": "{{note}}",
                    "status": "enable",
                    "payment": "cash",
                    "is_pay": "N",
                    "phone": "0919900398",
                    "order_price_total": "350",
                    "updated_at": "2018-03-03 13:40:09",
                    "created_at": "2018-03-03 13:40:09",
                    "machine_short_name": "{{short_name}}",
                    "location_name": "{{location_name}}",
                    "forder_name": "{{forder_name}}",
                    "code": "weqewqeqw",
                    "orders": [
                        {
                            "id": "51303087352188928",
                            "file_id": "50619024463237120",
                            "order_id": "51303087310245888",
                            "num": "10",
                            "price": "25",
                            "file_price_total": "250",
                            "materials_id": "1",
                            "size_types_id": "1",
                            "width": "1",
                            "height": "1",
                            "note": "{{meta_description}}",
                            "updated_at": "2018-03-03 13:40:09",
                            "created_at": "2018-03-03 13:40:09",
                            "file_link": [
                                "order/15198925436113141_海_0919900398_1X60_25元_新北科大__下載2.jpeg.jpeg"
                            ],
                            "material_name": "海",
                            "size_type_short_name": "A0"
                        },
                        {
                            "id": "51303087364771840",
                            "file_id": "50619024463237120",
                            "order_id": "51303087310245888",
                            "num": "10",
                            "price": "10",
                            "file_price_total": "100",
                            "materials_id": "2",
                            "size_types_id": "1",
                            "width": "1",
                            "height": "1",
                            "note": "{{meta_description}}",
                            "updated_at": "2018-03-03 13:40:09",
                            "created_at": "2018-03-03 13:40:09",
                            "file_link": [
                                "order/15198925436113141_海_0919900398_1X60_25元_新北科大__下載2.jpeg.jpeg"
                            ],
                            "material_name": "彩",
                            "size_type_short_name": "A0"
                        }
                    ]
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}

     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */


    /**
     * 查訂單全列
     *
     * @api {GET} /order/list/{order_way}/{page}    03.查訂單全列
     * @apiVersion 0.1.0
     * @apiDescription ・ 查訂單全列
     * @apiName GetOrderByAll
     * @apiGroup Orders
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                               [order_way ="ASC"]                       Id
     * @apiParam {int}                               [page =-1]                       Id
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.order          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "order": {
                    "id": "51303087310245888",
                    "machine_id": "1",
                    "note": "{{note}}",
                    "status": "enable",
                    "payment": "cash",
                    "is_pay": "N",
                    "phone": "0919900398",
                    "order_price_total": "350",
                    "updated_at": "2018-03-03 13:40:09",
                    "created_at": "2018-03-03 13:40:09",
                    "machine_short_name": "{{short_name}}",
                    "location_name": "{{location_name}}",
                    "forder_name": "{{forder_name}}",
                    "code": "weqewqeqw",
                    "orders": [
                        {
                            "id": "51303087352188928",
                            "file_id": "50619024463237120",
                            "order_id": "51303087310245888",
                            "num": "10",
                            "price": "25",
                            "file_price_total": "250",
                            "materials_id": "1",
                            "size_types_id": "1",
                            "width": "1",
                            "height": "1",
                            "note": "{{meta_description}}",
                            "updated_at": "2018-03-03 13:40:09",
                            "created_at": "2018-03-03 13:40:09",
                            "file_link": [
                                "order/15198925436113141_海_0919900398_1X60_25元_新北科大__下載2.jpeg.jpeg"
                            ],
                            "material_name": "海",
                            "size_type_short_name": "A0"
                        },
                        {
                            "id": "51303087364771840",
                            "file_id": "50619024463237120",
                            "order_id": "51303087310245888",
                            "num": "10",
                            "price": "10",
                            "file_price_total": "100",
                            "materials_id": "2",
                            "size_types_id": "1",
                            "width": "1",
                            "height": "1",
                            "note": "{{meta_description}}",
                            "updated_at": "2018-03-03 13:40:09",
                            "created_at": "2018-03-03 13:40:09",
                            "file_link": [
                                "order/15198925436113141_海_0919900398_1X60_25元_新北科大__下載2.jpeg.jpeg"
                            ],
                            "material_name": "彩",
                            "size_type_short_name": "A0"
                        }
                    ]
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}

     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */

    /**
     * 查訂ByPhone
     *
     * @api {GET} /order/phone/{phone}  04. 查訂ByPhone
     * @apiVersion 0.1.0
     * @apiDescription ・ 查訂ByPhone
     * @apiName GetOrderByPhone
     * @apiGroup Orders
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               phone                       Phone
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.order          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "order": {
                    "id": "51303087310245888",
                    "machine_id": "1",
                    "note": "{{note}}",
                    "status": "enable",
                    "payment": "cash",
                    "is_pay": "N",
                    "phone": "0919900398",
                    "order_price_total": "350",
                    "updated_at": "2018-03-03 13:40:09",
                    "created_at": "2018-03-03 13:40:09",
                    "machine_short_name": "{{short_name}}",
                    "location_name": "{{location_name}}",
                    "forder_name": "{{forder_name}}",
                    "code": "weqewqeqw",
                    "orders": [
                        {
                            "id": "51303087352188928",
                            "file_id": "50619024463237120",
                            "order_id": "51303087310245888",
                            "num": "10",
                            "price": "25",
                            "file_price_total": "250",
                            "materials_id": "1",
                            "size_types_id": "1",
                            "width": "1",
                            "height": "1",
                            "note": "{{meta_description}}",
                            "updated_at": "2018-03-03 13:40:09",
                            "created_at": "2018-03-03 13:40:09",
                            "file_link": [
                                "order/15198925436113141_海_0919900398_1X60_25元_新北科大__下載2.jpeg.jpeg"
                            ],
                            "material_name": "海",
                            "size_type_short_name": "A0"
                        },
                        {
                            "id": "51303087364771840",
                            "file_id": "50619024463237120",
                            "order_id": "51303087310245888",
                            "num": "10",
                            "price": "10",
                            "file_price_total": "100",
                            "materials_id": "2",
                            "size_types_id": "1",
                            "width": "1",
                            "height": "1",
                            "note": "{{meta_description}}",
                            "updated_at": "2018-03-03 13:40:09",
                            "created_at": "2018-03-03 13:40:09",
                            "file_link": [
                                "order/15198925436113141_海_0919900398_1X60_25元_新北科大__下載2.jpeg.jpeg"
                            ],
                            "material_name": "彩",
                            "size_type_short_name": "A0"
                        }
                    ]
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}

     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */
