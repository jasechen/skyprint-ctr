<?php

namespace App\Docs;

    /**
     * 新增材質
     *
     * #api {POST} /material    01. 新增材質
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增材質
     * @apiName Postmaterial
     * @apiGroup material
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {string}                            name                        名稱
     * @apiParam {string}                            short_name                        縮寫
     * @apiParam {string}                            zone                        地區
     * @apiParam {string}                            county                        縣市
     * @apiParam {string}                            location_name               地址（全名,唯一）
     * @apiParam {string}                            forder_name                 設備資料夾名稱（唯一）
     * @apiParam {string}                            code                        code（唯一）
     * @apiParam {string=enable,disable,delete}                            [status="enable"]                        狀態
     *
     * @apiParamExample {json} Request
        {
            "name" : "{{name}}",
            "short_name" : "{{short_name}}",
            "zone" : "{{zone}}",
            "county" : "{{county}}",
            "location_name" : "21212{{location_name}}",
            "status" : "{{status}}",
            "forder_name" : "{{forder_name}}1111",
            "code" : "ewqewqwww"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.material_id     機器id
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 201,
            "comment": "update success",
            "data": {
                "session": "1a59f702daee131f876e03baa66b2b84",
                "material_id": "52158074437373952"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */



    /**
     * 查材質ById
     *
     * @api {GET} /material/{id}    02.查材質ById
     * @apiVersion 0.1.0
     * @apiDescription ・ 查機器ById
     * @apiName GetmaterialByid
     * @apiGroup material
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               [id]                       Id
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.ㄇㄩㄠ          機器資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "material": {
                    "id": "1",
                    "name": "海報",
                    "short_name": "海",
                    "price": "25",
                    "code": "poster",
                    "updated_at": "2018-03-03 16:21:31",
                    "created_at": "2018-03-01 21:24:14"
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */


    /**
     * 查詢材質list
     *
     * @api {GET} /material/list/{order_way}/{page}    03.查詢材質list
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢材質list
     * @apiName GetmaterialList
     * @apiGroup material
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {string}                               [order_way ="ASC"]                       Id
     * @apiParam {int}                               [page =-1]                       Id
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.materials       機器資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "material": {
                    "id": "1",
                    "name": "海報",
                    "short_name": "海",
                    "price": "25",
                    "code": "poster",
                    "updated_at": "2018-03-03 16:21:31",
                    "created_at": "2018-03-01 21:24:14"
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */



    /**
     * 刪除ById
     *
     * #api {DELETE} /material/{id}     04.刪除這個材質byId
     * @apiVersion 0.1.0
     * @apiDescription ・ 刪除機器byId
     * @apiName DeletematerialById
     * @apiGroup material
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               phone                       Phone
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.material          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "delete success",
            "data": {
                "session": "1a59f702daee131f876e03baa66b2b84",
                "material_id": "1"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }
     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */


    /**
     * 查詢材質ByCode
     *
     * @api {GET} /material/code/{code} 05.查詢材質ByCode
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢材質ByCode
     * @apiName GetmaterialByCode
     * @apiGroup material
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               phone                       Phone
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.material          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "material": {
                    "id": "1",
                    "name": "海報",
                    "short_name": "海",
                    "price": "25",
                    "code": "poster",
                    "updated_at": "2018-03-03 16:21:31",
                    "created_at": "2018-03-01 21:24:14"
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */

    /**
     * 修改設備資訊
     *
     * #api {PUT} /material/{id}     06.修改設備資訊
     * @apiVersion 0.1.0
     * @apiDescription ・ 修改設備資訊
     * @apiName PutmaterialById
     * @apiGroup material
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               phone                       Phone
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.material          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "name" : "{{name}}",
            "short_name" : "{{short_name}}",
            "zone" : "{{zone}}",
            "county" : "{{county}}",
            "location_name" : "{{location_name}}",
            "status" : "{{status}}",
            "forder_name" : "{{forder_name}}",
            "code" : "weqewqeqw"
        }

     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */