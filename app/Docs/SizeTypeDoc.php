<?php

namespace App\Docs;

    /**
     * 新增尺寸
     *
     * #api {POST} /sizeType    01. 新增尺寸
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增尺寸
     * @apiName PostsizeType
     * @apiGroup sizeType
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {string}                            name                        名稱
     * @apiParam {string}                            short_name                        縮寫
     * @apiParam {string}                            zone                        地區
     * @apiParam {string}                            county                        縣市
     * @apiParam {string}                            location_name               地址（全名,唯一）
     * @apiParam {string}                            forder_name                 設備資料夾名稱（唯一）
     * @apiParam {string}                            code                        code（唯一）
     * @apiParam {string=enable,disable,delete}                            [status="enable"]                        狀態
     *
     * @apiParamExample {json} Request
        {
            "name" : "{{name}}",
            "short_name" : "{{short_name}}",
            "zone" : "{{zone}}",
            "county" : "{{county}}",
            "location_name" : "21212{{location_name}}",
            "status" : "{{status}}",
            "forder_name" : "{{forder_name}}1111",
            "code" : "ewqewqwww"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.sizeType_id     機器id
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 201,
            "comment": "update success",
            "data": {
                "session": "1a59f702daee131f876e03baa66b2b84",
                "sizeType_id": "52158074437373952"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */



    /**
     * 查尺寸ById
     *
     * @api {GET} /sizetype/{id}    02.查尺寸ById
     * @apiVersion 0.1.0
     * @apiDescription ・ 查尺寸ById
     * @apiName GetsizeTypeByid
     * @apiGroup sizeType
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               [id]                       Id
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.ㄇㄩㄠ          機器資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "material": {
                    "id": "1",
                    "name": "A0",
                    "short_name": "A0",
                    "width": "1",
                    "height": "1",
                    "code": "A0",
                    "updated_at": "2018-03-03 16:25:28",
                    "created_at": "2018-03-01 21:34:19"
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */


    /**
     * 查詢尺寸list
     *
     * @api {GET} /sizetype/list/{order_way}/{page}    03.查詢尺寸list
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢尺寸list
     * @apiName GetsizeTypeList
     * @apiGroup sizeType
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {string}                               [order_way ="ASC"]                       Id
     * @apiParam {int}                               [page =-1]                       Id
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.sizeTypes       機器資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "session": "1a59f702daee131f876e03baa66b2b84",
                "sizetypes": [
                    {
                        "id": "1",
                        "name": "A0",
                        "short_name": "A0",
                        "width": "1",
                        "height": "1",
                        "code": "A0",
                        "updated_at": "2018-03-03 16:25:28",
                        "created_at": "2018-03-01 21:34:19"
                    },
                    {
                        "id": "2",
                        "name": "A1",
                        "short_name": "A1",
                        "width": "1",
                        "height": "1",
                        "code": "A1",
                        "updated_at": "2018-03-03 16:25:30",
                        "created_at": "2018-03-01 21:34:19"
                    },
                    {
                        "id": "3",
                        "name": "A2",
                        "short_name": "A2",
                        "width": "1",
                        "height": "1",
                        "code": "A2",
                        "updated_at": "2018-03-03 16:25:32",
                        "created_at": "2018-03-01 21:34:19"
                    },
                    {
                        "id": "4",
                        "name": "B1",
                        "short_name": "B1",
                        "width": "1",
                        "height": "1",
                        "code": "B1",
                        "updated_at": "2018-03-03 16:25:34",
                        "created_at": "2018-03-01 21:34:19"
                    },
                    {
                        "id": "5",
                        "name": "B2",
                        "short_name": "B2",
                        "width": "1",
                        "height": "1",
                        "code": "B2",
                        "updated_at": "2018-03-03 16:25:35",
                        "created_at": "2018-03-01 21:34:19"
                    },
                    {
                        "id": "6",
                        "name": "90180",
                        "short_name": "90180",
                        "width": "1",
                        "height": "1",
                        "code": "90180",
                        "updated_at": "2018-03-03 16:25:36",
                        "created_at": "2018-03-01 21:34:19"
                    },
                    {
                        "id": "7",
                        "name": "After",
                        "short_name": "After",
                        "width": "1",
                        "height": "1",
                        "code": "After",
                        "updated_at": "2018-03-03 16:25:37",
                        "created_at": "2018-03-01 21:34:19"
                    },
                    {
                        "id": "8",
                        "name": "Cust",
                        "short_name": "Cust",
                        "width": "1",
                        "height": "1",
                        "code": "Cust",
                        "updated_at": "2018-03-03 16:25:39",
                        "created_at": "2018-03-01 21:34:19"
                    }
                ]
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */



    /**
     * 刪除ById
     *
     * #api {DELETE} /sizeType/{id}     04.刪除這個材質byId
     * @apiVersion 0.1.0
     * @apiDescription ・ 刪除機器byId
     * @apiName DeletesizeTypeById
     * @apiGroup sizeType
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               phone                       Phone
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.sizeType          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "delete success",
            "data": {
                "session": "1a59f702daee131f876e03baa66b2b84",
                "sizeType_id": "1"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }
     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */


    /**
     * 查詢材質ByCode
     *
     * @api {GET} /sizeType/code/{code} 05.查詢材質ByCode
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢材質ByCode
     * @apiName GetsizeTypeByCode
     * @apiGroup sizeType
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8"
        }
     *
     * @apiParam {int}                               phone                       Phone
     *
     * @apiParamExample {json} Request
        {

        }
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.sizeType          訂單資訊
     *
     * @apiSuccessExample {json}    Response: 201
        {
            "status": "success",
            "code": 200,
            "comment": "find success",
            "data": {
                "sizeType": {
                    "id": "1",
                    "name": "海報",
                    "short_name": "海",
                    "price": "25",
                    "code": "poster",
                    "updated_at": "2018-03-03 16:21:31",
                    "created_at": "2018-03-01 21:24:14"
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "session empty"
        }
     *
     * @apiErrorExample {json}  Response: 410.01
        {
            "status": "fail",
            "code": 410,
            "comment": "session is NOT alive"
        }

     * @apiErrorExample {json}  Response: 422.02
        {
            "status": "fail",
            "code": 422,
            "comment": "status error"
        }
     *
     * @apiErrorExample {json}  Response: 422.03
        {
            "status": "fail",
            "code": 422,
            "comment": "file error"
        }
     *
     * @apiErrorExample {json}  Response: 500.01
        {
            "status": "fail",
            "code": 500,
            "comment": "create error"
        }
     *
     */

    /**