<?php

namespace App\Docs;

    /**
     * 初始化 session
     *
     * @api {POST} /session/init 01. 初始化
     * @apiVersion 0.1.0
     * @apiDescription ・ 用 device_code, device_os, device_type, device_lang, device_token, device_channel 初始化 session<br /> ・ 若不傳 device_code 或為空值，則自行產生<br /> ・ 若不傳 device_channel 或為空值，則依 device_os 產生相對應的值
     * @apiName PostSessionInit
     * @apiGroup Session
     *
     * @apiParam {string}                                           [device_code]             裝置代碼
     * @apiParam {string=ios, andriod, macos, windows, others}      [device_os="others"]      裝置作業系統
     * @apiParam {string=iphone, ipad, mobile, pc, others}          [device_type="others"]    裝置型態
     * @apiParam {string=zh-tw, zh-cn, jp, en}                      [device_lang="en"]        裝置語言
     * @apiParam {string}                                           [device_token]            裝置 token
     * @apiParam {string=apns,gcm,baidu,others}                     [device_channel]          裝置 訊息頻道
     *
     * @apiParamExample {json} Request
{
    "device_code" : "b4233639-0a61-4b0f-9ab3-682066ded25a",
    "device_os"   : "ios",
    "device_type" : "iphone" ,
    "device_lang" : "en",
    "device_token" : "b4233639-0a61-4b0f-9ab3-682066ded25a",
    "device_channel" : "apns"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.device_code    裝置代碼
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "init success",
    "data": {
        "device_code": "aeb0c92d-c9fa-47ea-814e-8e009c1d3056",
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "device os error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "device type error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "device lang error"
}
     *
     * @apiErrorExample {json}  Response: 422.04
{
    "status": "fail",
    "code": 422,
    "comment": "device channel error"
}
     *
     */


// ===
    /**
     * 查詢 session
     *
     * @api {GET} /session/{code} 02. 用 code 查詢 session
     * @apiVersion 0.1.0
     * @apiDescription ・ 用 code 查詢 session。
     * @apiName GetSessionFindByCode
     * @apiGroup Session
     *
     * @apiParam {string}    code       Session 代碼
     *
     * @apiParamExample {json} Request
{
    "code" : "8f3e973061800dc6ebcb367079b305d8",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     * @apiSuccess (Success) {string}   data.status    Session 狀態
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "status": "login"
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
     *
     */
