<?php

namespace App\Services;

use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use App\Repositories\FileRepo;


/**
 * Class FileServ
 *
 * @package namespace App\Services;
 */
class FileServ
{


    public function __construct()
    {

        $this->fileRepo = new FileRepo();
    } // END function


    public function moveFileTo($file, $target, $source = '',$newFileName)
    {
        $source .= empty($source) ? '' : '/';
        $exists = Storage::disk('files')->exists($source . $file);

        if (empty($exists)) {return false;}

        Storage::disk('files')->move($source . $file, $target . '/'. $newFileName);
        return true;
    } // END function


    /**
     * Covert image
     *
     * @param  $parentType
     * @param  \Illuminate\Filesystem\Filesystem  $file
     *
     * @throws
     * @return string   $filename
     */
    public function convertImage($parentType, $file, $extension = null)
    {

        $sizes = config('image.sizes');
        $savePath  = public_path('gallery/' . $parentType);

        $filename = Carbon::now()->timestamp . mt_rand(100000, 999999);
        $fileExtension = empty($extension) ? $file->extension() : $extension;

        if (!empty($extension)) {
            $convertFile = file_get_contents($file);

            if ($convertFile === FALSE) {
                return false;
            } // END if
        } else {
            $convertFile = $file;
        } // END if

        $img = Image::make($convertFile);

        $filePath = $savePath . '/' . $filename . '_o.' . $fileExtension;
        $img->save($filePath);

        $imgWidth = $img->width();

        foreach ($sizes as $id => $size) {
            if ($imgWidth < $size['width']) {
                continue;
            } // END if

            if ($size['width'] == $size['height']) {
                $img->fit($size['width']);
            } else {
                $img->resize($size['width'], $size['height'], function ($constraint) {
                    $constraint->aspectRatio();
                });
            } // END if else

            $filePath = $savePath . '/' . $filename . '_' . $id . '.' . $fileExtension;
            $img->save($filePath);
        } // END foreach

        return $filename;
    } // END function


    public function findLinks($filename)
    {

        $fileDatum = $this->findByFilename($filename);

        if ($fileDatum->isEmpty()) {
            return [];
        } // END if

        $links = [];
        $filename = $fileDatum->first()->filename;
        $fileExtension = $fileDatum->first()->extension;
        $fileStatus = $fileDatum->first()->status;

        $path = $fileStatus.'/';
        $links[] = $path . $filename.'.'.$fileExtension;

        return $links;
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->fileRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->fileRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $sessionId
     * @param $filename
     * @param $extension
     * @param $mimeType
     * @param $size
     * @param $type
     * @param $status
     *
     * @return
     */
    public function create($sessionId, $filename,$fileOriginalName, $extension, $mimeType, $size, $type = 'image', $status = 'upload')
    {
        $data = [
                 'session_id' => $sessionId,
                 'filename'  => $filename,
                 'original_file_name'  => $fileOriginalName,
                 'extension' => $extension,
                 'mime_type' => $mimeType,
                 'size'   => $size,
                 'type'   => $type,
                 'status' => $status
        ];

        return $this->fileRepo->createData($data);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByStatus($status, $orderby = [], $page = -1, $numItems = 10)
    {
        $where = ['status' => $status];

        return $this->fileRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByFilename
     *
     * @param $filename
     *
     * @return
     */
    public function findByFilename($filename)
    {
        $where = ['filename' => $filename];

        return $this->fileRepo->fetchDatum($where);
    } // END function


    /*
     * fetchDatum
     *
     * @param $where
     *
     * @return
     */
    public function fetchDatum($where)
    {
        return $this->fileRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->fileRepo->fetchDatum($where);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByMachineIdAndStatus($status = '' , $machineId = '', $orderby = [], $page = -1, $numItems = 10)
    {
        $bindValues = [];
        $query  = "SELECT b.machine_id, a.* ";
        $query .= "from files as a ";
        $query .= "left join orders as b on a.order_id = b.id ";
        $query .= "";
        $query .= "WHERE 1 ";

        if (!empty($status) and  !empty($machineId)) {
            $query .= "AND a.status = '$status' And b.machine_id = '$machineId' ";
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->fileRepo->fetch($query, $bindValues);
    } // END function

}
