<?php

namespace App\Services;

//use App\Repositories\CronJobRepo;


/**
 * Class CronJobServ
 *
 * @package namespace App\Services;
 */
class CronJobServ
{


    public function __construct()
    {

        //$this->cronJobRepo = new CronJobRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->cronJobRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->cronJobRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $categoryId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $coverTitle
     * @param $coverAlt
     *
     * @return
     */
    public function create($_data)
    {
        $data = ['name' => $_data['name'],
                'short_name' => $_data['short_name'],
                'zone' => $_data['zone'],
                'county' => $_data['county'],
                'location_name' => $_data['location_name'],
                'status' => $_data['status'],
                'forder_name' => $_data['forder_name'],
                'code' => $_data['code']
        ];

        return $this->cronJobRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {
        return $this->cronJobRepo->fetchData([], $orderby, $page, $numItems);
    } // END function




    /*
     * findByForderName
     *
     * @param $forderName
     *
     * @return
     */
    public function findByForderName($forderName)
    {
        $where = ['forder_name' => $forderName];

        return $this->cronJobRepo->fetchDatum($where);
    } // END function


    /*
     * findByLocationName
     *
     * @param $locationName
     *
     * @return
     */
    public function findByLocationName($locationName)
    {
        $where = ['location_name' => $locationName];

        return $this->cronJobRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->cronJobRepo->fetchDatum($where);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->cronJobRepo->fetchDatum($where);
    } // END function

}
