<?php

namespace App\Services;

use App\Repositories\FilesLogRepo;


/**
 * Class FilesLogServ
 *
 * @package namespace App\Services;
 */
class FilesLogServ
{


    public function __construct()
    {

        $this->filesLogRepo = new FilesLogRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->FilesLogRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->FilesLogRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $filename
     * @param $file_id
     * @param $status
     *
     * @return
     */
    public function create($_data)
    {
        $data = [
                'filename' => $_data['filename'],
                'file_id' => $_data['file_id'],
                'status' => $_data['status'],
        ];

        return $this->filesLogRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {
        return $this->FilesLogRepo->fetchData([], $orderby, $page, $numItems);
    } // END function




    /*
     * findByForderName
     *
     * @param $forderName
     *
     * @return
     */
    public function findByForderName($forderName)
    {
        $where = ['forder_name' => $forderName];

        return $this->FilesLogRepo->fetchDatum($where);
    } // END function


    /*
     * findByLocationName
     *
     * @param $locationName
     *
     * @return
     */
    public function findByLocationName($locationName)
    {
        $where = ['location_name' => $locationName];

        return $this->FilesLogRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->FilesLogRepo->fetchDatum($where);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->FilesLogRepo->fetchDatum($where);
    } // END function

}
