<?php

namespace App\Services;

use App\Repositories\cornJobLogRepo;


/**
 * Class cornJobLogServ
 * 
 * @package namespace App\Services;
 */
class CornJobLogServ
{


    public function __construct()
    {

        $this->cornJobLogRepo = new CornJobLogRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->cornJobLogRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->cornJobLogRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $status
     * @param $job_status
     *
     * @return
     */
    public function create($_data)
    {
        $data = [
                'status' => $_data['status'],
                'job_status' => $_data['job_status'],
        ];

        return $this->cornJobLogRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {
        return $this->cornJobLogRepo->fetchData([], $orderby, $page, $numItems);
    } // END function




    /*
     * findByForderName
     *
     * @param $forderName
     *
     * @return
     */
    public function findByForderName($forderName)
    {
        $where = ['forder_name' => $forderName];

        return $this->cornJobLogRepo->fetchDatum($where);
    } // END function


    /*
     * findByLocationName
     *
     * @param $locationName
     *
     * @return
     */
    public function findByLocationName($locationName)
    {
        $where = ['location_name' => $locationName];

        return $this->cornJobLogRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->cornJobLogRepo->fetchDatum($where);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->cornJobLogRepo->fetchDatum($where);
    } // END function

}
