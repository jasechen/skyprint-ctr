<?php

namespace App\Services;

use App\Repositories\MaterialRepo;


/**
 * Class MaterialServ
 *
 * @package namespace App\Services;
 */
class MaterialServ
{


    public function __construct()
    {

        $this->materialRepo = new MaterialRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->materialRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->materialRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $categoryId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $coverTitle
     * @param $coverAlt
     *
     * @return
     */
    public function create($categoryId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt)
    {
        $data = ['category_id' => $categoryId,
                'slug' => $slug,
                'excerpt' => $excerpt,
                'og_title' => $ogTitle,
                'og_description' => $ogDescription,
                'meta_title' => $metaTitle,
                'meta_description' => $metaDescription,
                'cover_title' => $coverTitle,
                'cover_alt' => $coverAlt
        ];

        return $this->materialRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {
        return $this->materialRepo->fetchData([], $orderby, $page, $numItems);
    } // END function

    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->materialRepo->fetchDatum($where);
    } // END function

    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->materialRepo->fetchDatum($where);
    } // END function

}
