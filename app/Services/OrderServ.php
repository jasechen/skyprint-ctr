<?php

namespace App\Services;

use App\Repositories\OrderRepo;
use App\Repositories\OrderItemRepo;

use App\Services\FileServ;
use App\Services\MachineServ;
use App\Services\MaterialServ;
use App\Services\SizeTypeServ;


/**
 * Class OrderServ
 *
 * @package namespace App\Services;
 */
class OrderServ
{


    public function __construct()
    {
        $this->orderRepo = new OrderRepo();
        $this->orderItemRepo = new OrderItemRepo();

        $this->fileServ = new FileServ();
        $this->machineServ = new MachineServ();
        $this->materialServ = new MaterialServ();
        $this->sizeTypeServ = new SizeTypeServ();
    } // END function


    /*
     * updateNumPosts
     *
     * @param $categoryId
     * @param $num
     *
     * @return
     */
    public function updateNumPosts($categoryId, $num = 1)
    {

        $datum = $this->findById($categoryId);

        if ($datum->isEmpty()) {
            return false;
        } // END if

        $updateData  = ['num_posts' => intval($datum->first()->num_posts) + $num];
        $updateWhere = ['id' => $categoryId];

        return $this->update($updateData, $updateWhere);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->orderRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->orderRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $machine_id
     * @param $status
     * @param $payment
     * @param $note
     * @param $phone
     * @param $is_pay
     *
     * $machineId,$status,$payment,$note,$phone,$isPay
     * @return
     */
    
    public function create($machineId,$status = 'enable',$payment='cash',$note='',$phone,$isPay='0',$sessionCode)
    {
        $data = [
            'machine_id' => $machineId,
            'status' => $status,
            'payment' => $payment,
            'note' => $note,
            'phone' => $phone,
            'is_pay' => $isPay,
            'session_id' => $sessionCode,
        ];

        return $this->orderRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($status = '', $orderby = [], $page = -1, $numItems = 10)
    {
        $bindValues = [];
        $query  = "SELECT o.* ";
        $query .= " ";
        $query .= "FROM orders AS o ";
        $query .= "";
        $query .= "WHERE 1 ";

        if (!empty($status)) {
            $query .= "AND o.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->orderRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findByName
     *
     * @param $name
     *
     * @return
     */
    public function findByName($name)
    {
        $where = ['name' => $name];
        $datum = $this->orderRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $orderItemWhere = ['category_id' => $datum->first()->id];
        $orderItemDatum = $this->orderDatasRepo->fetchDatum($orderItemWhere);

        $datum->first()->slug    = $orderItemDatum->first()->slug;
        $datum->first()->excerpt = $orderItemDatum->first()->excerpt;
        $datum->first()->og_title       = $orderItemDatum->first()->og_title;
        $datum->first()->og_description = $orderItemDatum->first()->og_description;
        $datum->first()->meta_title       = $orderItemDatum->first()->meta_title;
        $datum->first()->meta_description = $orderItemDatum->first()->meta_description;
        $datum->first()->cover_title = $orderItemDatum->first()->cover_title;
        $datum->first()->cover_alt   = $orderItemDatum->first()->cover_alt;

        return $datum;
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];
        $datum = $this->orderRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if


        $machineId = $datum->first()->machine_id;
        $machineDatum = $this->machineServ->findById($machineId);


        $datum->first()->machine_short_name = $machineDatum->first()->short_name;
        $datum->first()->location_name = $machineDatum->first()->location_name;
        $datum->first()->forder_name = $machineDatum->first()->forder_name;
        $datum->first()->code = $machineDatum->first()->code;




        $orderItemWhere = ['order_id' => $datum->first()->id];
        $orderItemDatum = $this->orderItemRepo->fetchData($orderItemWhere);

        foreach($orderItemDatum as $key => $value){

            $fileId = $value->file_id;
            $orderId = $value->order_id;
            $materialsId = $value->materials_id;
            $sizeTypesId = $value->size_types_id;

            $sizeTypeServDatum = $this->sizeTypeServ->findById($sizeTypesId);
            $sizeTypeServDatum->first()->short_name;
            $materialDatum = $this->materialServ->findById($materialsId);
            $materialDatum->first()->short_name;

            $fileServDatum = $this->fileServ->findById($fileId);
            $filename =  $fileServDatum->first()->filename;
            $file_links =$this->fileServ->findLinks($filename);


            $value->file_link = $file_links;
            $value->material_name =$materialDatum->first()->short_name;
            $value->size_type_short_name =$sizeTypeServDatum->first()->short_name;
        }

        $datum->first()->orders = $orderItemDatum->toArray();
        return $datum;
    } // END function


    /*
     * findByIdToAll
     *
     * @param $id
     *
     * @return
     */
    public function findByIdToAll($id)
    {
        $where = ['id' => $id];
        $datum = $this->orderRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if


        $machineId = $datum->first()->machine_id;
        $machineDatum = $this->machineServ->findById($machineId);
        $datum->first()->machine_short_name = $machineDatum->first()->short_name;
        $datum->first()->location_name = $machineDatum->first()->location_name;
        $datum->first()->forder_name = $machineDatum->first()->forder_name;
        $datum->first()->code = $machineDatum->first()->code;




        $orderItemWhere = ['order_id' => $datum->first()->id];
        $orderItemDatum = $this->orderItemRepo->fetchData($orderItemWhere);

        foreach($orderItemDatum as $key => $value){

            $fileId = $value->file_id;
            $orderId = $value->order_id;
            $materialsId = $value->materials_id;
            $sizeTypesId = $value->size_types_id;

            $sizeTypeServDatum = $this->sizeTypeServ->findById($sizeTypesId);
            $sizeTypeServDatum->first()->short_name;
            $materialDatum = $this->materialServ->findById($materialsId);
            $materialDatum->first()->short_name;

            $fileServDatum = $this->fileServ->findById($fileId);
            $filename =  $fileServDatum->first()->filename;
            $file_links =$this->fileServ->findLinks($filename);


            $value->file_link = $file_links;
            $value->material_name =$materialDatum->first()->short_name;
            $value->size_type_short_name =$sizeTypeServDatum->first()->short_name;
        }

        $datum->first()->orders = $orderItemDatum->toArray();
        return $datum->first()->orders;
    } // END function

    public function findByPhone($phone)
    {

        $where = ['phone' => $phone];
        $data = $this->orderRepo->fetchData($where);
        if ($data->isEmpty()) {
            return $data;
        } // END if

        foreach ($data as $orderValue){
        $machineId = $orderValue->machine_id;
        $machineDatum = $this->machineServ->findById($machineId);

        $orderValue->machine_short_name = $machineDatum->first()->short_name;
        $orderValue->location_name = $machineDatum->first()->location_name;
        $orderValue->forder_name = $machineDatum->first()->forder_name;
        $orderValue->code = $machineDatum->first()->code;


        $orderItemWhere = ['order_id' => $orderValue->id];
        $orderItemDatum = $this->orderItemRepo->fetchData($orderItemWhere);

            foreach($orderItemDatum as $key => $value){

                $fileId = $value->file_id;
                $orderId = $value->order_id;
                $materialsId = $value->materials_id;
                $sizeTypesId = $value->size_types_id;

                $sizeTypeServDatum = $this->sizeTypeServ->findById($sizeTypesId);
                $sizeTypeServDatum->first()->short_name;
                $materialDatum = $this->materialServ->findById($materialsId);
                $materialDatum->first()->short_name;

                $fileServDatum = $this->fileServ->findById($fileId);
                $filename =  $fileServDatum->first()->filename;
                $file_links =$this->fileServ->findLinks($filename);
                $original_file_name = $fileServDatum->first()->original_file_name;


                $value->file_link = $file_links;
                $value->original_file_name = $original_file_name;
                $value->material_name =$materialDatum->first()->short_name;
                $value->size_type_short_name =$sizeTypeServDatum->first()->short_name;
            }
            $orderValue->orders = $orderItemDatum->toArray();
            $orderItemDatum = null;
    }
        return $data->toArray();
    } // END function

}
