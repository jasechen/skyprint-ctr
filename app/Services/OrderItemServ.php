<?php

namespace App\Services;

use App\Repositories\OrderItemRepo;


/**
 * Class OrderItemsServ
 *
 * @package namespace App\Services;
 */
class OrderItemServ
{


    public function __construct()
    {

        $this->orderItemRepo = new OrderItemRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->orderItemRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->orderItemRepo->updateData($data, $where);
    } // END function


    /*
     * getPrice
     *
     * @param $num
     * @param $price
     * @param $sNum
     *
     * @return
     */
    
    public function getPrice($num,$price,$sNum)
    {
            //打折
            //假設 遇到時間打折
            //設 週一到週五打八折   星期打折
            //設 晚上6~10點打七折   時間打折
            //設
            //end 打折


        return $num * $price * $sNum;;
    } // END function


    /*
     * create
     *
     * @param $categoryId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $coverTitle
     * @param $coverAlt
     *
     * @return
     */
    public function create($orderId,$materialsId,$sizeTypesId,$fileId,$num,$price,$width,$height,$note,$filePriceTotal)
    {
        $data = ['order_id' => $orderId,
                'materials_id' => $materialsId,
                'size_types_id' => $sizeTypesId,
                'file_id' => $fileId,
                'width' => $width,
                'height' => $height,
                'note' => $note,
                'num' => $num,
                'price' => $price,
                'file_price_total' =>$filePriceTotal
        ];

        return $this->orderItemRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {
        return $this->orderItemRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->orderItemRepo->fetchDatum($where);
    } // END function

}
