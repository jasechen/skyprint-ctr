<?php

namespace App\Services;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Package\Jsonponse\Jsonponse;

/**
 * Class BaseServ
 *
 * @package namespace App\Services;
 */
class BaseServ
{


    public function __construct()
    {
    } // END function


    /**
     *
     */
    public function isNoEmpty($filedArr,$tmpArr)
    {
        foreach ($tmpArr as $value){
            if(!array_key_exists($value,$filedArr)){
                $code ='400';
                $comment =$value.' is empty';
                Jsonponse::fail($comment, $code);
            }
        }

        return true;
    } // END function


}
