<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareateFilesLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('log')->create('files_log', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->text('filename')->comment('檔案名稱');
            $table->text('file_id')->nullable()->comment('檔案id');
            $table->enum('status', ['upload','order', 'push', 'pull','finish','print','done','delete'])->default('upload')->comment('檔案狀態');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_log');
    }
}
