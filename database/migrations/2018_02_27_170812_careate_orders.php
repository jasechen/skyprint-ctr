<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->bigInteger('machine_id')->nullable()->comment('設備id');
            $table->unsignedBigInteger('session_id');
            $table->text('note')->nullable()->comment('備註');

            $table->enum('status', ['enable', 'disable', 'delete'])->default('enable')->comment('狀態');
            $table->enum('payment', ['cash', 'credit'])->default('cash')->comment('付費方式');
            $table->enum('is_pay', ['Y', 'N'])->default('N')->comment('是否付費');
            $table->char('phone', 12)->nullable()->comment('電話');
            $table->integer('order_price_total')->default('0')->comment('總價');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();



            $table->index('session_id');
            $table->index('phone');
            $table->index('machine_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
