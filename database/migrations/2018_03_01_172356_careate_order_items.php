<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareateOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->bigInteger('file_id')->nullable()->comment('檔案id');
            $table->bigInteger('order_id')->nullable()->comment('order_id');
            $table->integer('num')->nullable()->comment('數量');
            $table->integer('price')->nullable()->comment('單價');
            $table->integer('file_price_total')->nullable()->comment('該檔列印總價');
            $table->integer('materials_id')->nullable()->comment('材質id');
            $table->integer('size_types_id')->nullable()->comment('尺寸id');
            $table->integer('width')->default(0)->comment('寬度');
            $table->integer('height')->default(0)->comment('長度');
            $table->text('note')->nullable()->comment('備註');

            $table->index('file_id');
            $table->index('order_id');


            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_datas');
    }
}
