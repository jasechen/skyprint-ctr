<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('files', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('session_id');
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('order_items_id')->nullable();

            $table->text('filename');
            $table->text('original_file_name');
            $table->string('extension');
            $table->string('mime_type');
            $table->integer('size');
            $table->enum('type', ['image', 'video', 'audio', 'others'])->default('image');
            $table->enum('status', ['upload','order', 'push', 'pull','finish','print','done','delete'])->default('upload');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index(['order_items_id']);
            $table->index(['order_id']);
            $table->index('session_id');

            $table->index('type');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
