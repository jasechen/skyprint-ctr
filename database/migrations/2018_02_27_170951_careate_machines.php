<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareateMachines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->text('name')->nullable()->comment('設備名稱');
            $table->text('zone')->nullable()->comment('區域');
            $table->string('county')->comment('縣市');
            $table->text('location_name')->nullable()->comment('地址');
            $table->text('forder_name')->nullable()->comment('對應資料夾');
            $table->text('ftp_host')->nullable()->comment('ftp host');
            $table->text('ftp_username')->nullable()->comment('ftp username');
            $table->text('ftp_password')->nullable()->comment('ftp password');
            $table->string('code');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();
        
            $table->index('code');
            $table->index('county');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines');
    }
}
