<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareateCornJobLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('log')->create('corn_job_log', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->enum('status', ['success','fail'])->default('success')->comment('狀態');
            $table->enum('job_status', ['upload','order', 'push', 'pull','finish','print','done','delete'])->default('upload')->comment('job狀態');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index('status');
            $table->index('job_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corn_job_log');
    }
}
