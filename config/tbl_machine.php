<?php
$fields = [
    'MACHINE_STATUS_ENABLE'  => 'enable',
    'MACHINE_STATUS_DISABLE'  => 'disable',
    'MACHINE_STATUS_DELETE'  => 'delete',

];


$fields['DEFAULT_MACHINE_STATUS'] = $fields['MACHINE_STATUS_ENABLE'];
$fields['ORDERS_STATUS'] = [
    $fields['MACHINE_STATUS_ENABLE'],
    $fields['MACHINE_STATUS_DISABLE'],
    $fields['MACHINE_STATUS_DELETE']
];

return $fields;