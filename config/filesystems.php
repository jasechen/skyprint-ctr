<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'files' => [
            'driver' => 'local',
            'root' => public_path('files'),
            'url' => env('APP_URL').'/files',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

        's3-files-upload' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET_UPLOAD'),
        ],

        's3-files-order' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET_ORDER'),
        ],

        's3-files-convert' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET_CONVERT'),
        ],

        's3-files-delivery' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET_DELIVERY'),
        ],

        'ftp-convert' => [
            'driver'   => 'ftp',
            'host'     => env('CRON_JOB_FTP_CONVERT_HOST'),
            'username' => env('CRON_JOB_FTP_CONVERT_USER'),
            'password' => env('CRON_JOB_FTP_CONVERT_PASSWORD'),
            // Optional FTP Settings...
             'port'     => 21,
             'root'     => '/data/ftp/z',
             'passive'  => false,
             'ssl'      => false,
             'timeout'  => 60,
        ],

        'ftp-convert-done' => [
            'driver'   => 'ftp',
            'host'     => env('CRON_JOB_FTP_CONVERT_HOST'),
            'username' => env('CRON_JOB_FTP_CONVERT_USER'),
            'password' => env('CRON_JOB_FTP_CONVERT_PASSWORD'),
            // Optional FTP Settings...
             'port'     => 21,
             'root'     => '/data/ftp/a',
             'passive'  => false,
             'ssl'      => false,
             'timeout'  => 60,
        ],

        'ftp-sky' => [
            'driver'   => 'ftp',
            'host'     => env('CRON_JOB_FTP_CONVERT_HOST'),
            'username' => env('CRON_JOB_FTP_CONVERT_USER'),
            'password' => env('CRON_JOB_FTP_CONVERT_PASSWORD'),
            // Optional FTP Settings...
             'port'     => 21,
             'root'     => 'sky008',
             'passive'  => false,
             'ssl'      => false,
             'timeout'  => 60,
        ],

    ],

];
