<?php
$fields = [
    'SESSIONS_STATUS_INIT' => 'init',
    'SESSIONS_STATUS_LOGIN' => 'login',
    'SESSIONS_STATUS_LOGOUT' => 'logout',
];

$fields['DEFAULT_SESSIONS_STATUS'] = $fields['SESSIONS_STATUS_INIT'];
$fields['SESSIONS_STATUS'] = [
    $fields['SESSIONS_STATUS_INIT'],
    $fields['SESSIONS_STATUS_LOGIN'],
    $fields['SESSIONS_STATUS_LOGOUT']
];

return $fields;