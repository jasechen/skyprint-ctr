<?php
$fields = [
    'ORDERS_STATUS_ENABLE'  => 'enable',
    'ORDERS_STATUS_DISABLE'  => 'disable',
    'ORDERS_STATUS_DELETE'  => 'delete',
    'ORDERS_PAYMENT_CASH'  => 'cash',
    'ORDERS_PAYMENT_CREDIT'  => 'credit',
];


$fields['DEFAULT_ORDERS_STATUS'] = $fields['ORDERS_STATUS_ENABLE'];
$fields['ORDERS_STATUS'] = [
    $fields['ORDERS_STATUS_ENABLE'],
    $fields['ORDERS_STATUS_DISABLE'],
    $fields['ORDERS_STATUS_DELETE']
];


$fields['DEFAULT_ORDERS_PAYMENT'] = $fields['ORDERS_PAYMENT_CASH'];
$fields['ORDERS_PAYMENT'] = [
    $fields['ORDERS_PAYMENT_CASH'],
    $fields['ORDERS_PAYMENT_CREDIT']
];

return $fields;